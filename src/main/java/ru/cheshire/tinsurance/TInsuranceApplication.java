package ru.cheshire.tinsurance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TInsuranceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TInsuranceApplication.class, args);
    }
}
