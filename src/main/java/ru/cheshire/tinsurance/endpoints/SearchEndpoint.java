package ru.cheshire.tinsurance.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.cheshire.tinsurance.service.fileHandler.i.FileSearchService;
import ru.cheshire.tinsurance.soap.GetNumberRequest;
import ru.cheshire.tinsurance.soap.GetNumberResponse;

@Endpoint
public class SearchEndpoint {
    private static final String NAMESPACE_URI = "http://tinsurance.cheshire.ru/soap";

    private FileSearchService fileSearchService;

    @Autowired
    public SearchEndpoint(FileSearchService fileSearchService) {
        this.fileSearchService = fileSearchService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getNumberRequest")
    @ResponsePayload
    public GetNumberResponse findNumber(@RequestPayload GetNumberRequest request) {
        GetNumberResponse response = new GetNumberResponse();
        response.setResult(fileSearchService.findNumber(request.getNumber()));
        return response;
    }
}
