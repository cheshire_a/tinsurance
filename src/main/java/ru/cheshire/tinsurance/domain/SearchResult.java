package ru.cheshire.tinsurance.domain;

import ru.cheshire.tinsurance.soap.Result;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "search_result")
public class SearchResult/* extends Result */ {

    @Transient
    public static final String RESULT_CODE_OK = "00.Result.OK";
    @Transient
    public static final String RESULT_CODE_ERROR = "02.Result.Error";
    @Transient
    public static final String RESULT_CODE_NOT_FOUND = "01.Result.NotFound";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer number;
    private String code;
    private String fileNames;
    private String error;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public SearchResult() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFileNames() {
        return fileNames;
    }

    public void setFileNames(List<String> fileNames) {
        this.fileNames = String.join(",", fileNames);
    }

    public void setFileNames(String fileNames) {
        this.fileNames = fileNames;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public SearchResult(Result result, Integer number) {
        this.code = result.getCode();
        this.fileNames = String.join(",", result.getFileNames());
        this.error = result.getError();
        this.number = number;
    }

}
