package ru.cheshire.tinsurance.service.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.cheshire.tinsurance.domain.SearchResult;

@Repository
public interface SearchResultRepository extends CrudRepository<SearchResult, Long> {

}
