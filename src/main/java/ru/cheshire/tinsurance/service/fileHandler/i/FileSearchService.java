package ru.cheshire.tinsurance.service.fileHandler.i;

import ru.cheshire.tinsurance.soap.Result;

public interface FileSearchService {
    Result findNumber(Integer number);
}
