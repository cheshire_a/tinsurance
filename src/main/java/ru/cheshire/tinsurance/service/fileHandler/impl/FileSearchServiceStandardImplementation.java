package ru.cheshire.tinsurance.service.fileHandler.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.cheshire.tinsurance.configuration.properties.FileGenerationProperties;
import ru.cheshire.tinsurance.domain.SearchResult;
import ru.cheshire.tinsurance.service.fileHandler.i.FileSearchService;
import ru.cheshire.tinsurance.service.repo.SearchResultRepository;
import ru.cheshire.tinsurance.soap.Result;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.concurrent.*;
import java.util.function.BinaryOperator;
import java.util.stream.IntStream;

@Service
public class FileSearchServiceStandardImplementation implements FileSearchService {
    private final Logger logger = LoggerFactory.getLogger(FileSearchServiceStandardImplementation.class);

    private FileGenerationProperties fileGenerationProperties;

    private SearchResultRepository searchResultRepository;

    @Autowired
    public FileSearchServiceStandardImplementation(FileGenerationProperties generationProperties,
                                                   SearchResultRepository searchResultRepository) {
        this.fileGenerationProperties = generationProperties;
        this.searchResultRepository = searchResultRepository;
    }

    private class InFileNumSeeker implements Callable<Result> {

        private final Path path;
        private final String targetNumber;

        InFileNumSeeker(Path path, String targetNumber) {
            this.path = path;
            this.targetNumber = targetNumber;
        }

        @Override
        public Result call() {
            long begin = System.nanoTime();
            Result result = new Result();

            try {
                //TODO сделать перевод в mapFileChannel всех файлов, а потом параллелить

                FileChannel fileChannel = FileChannel.open(path, StandardOpenOption.READ);
                MappedByteBuffer map = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());


                int currentMatchedPosition = 0;
                StringBuilder resultedString = new StringBuilder();
                while (map.hasRemaining()) {
                    char readChar = (char) map.get();
                    if (targetNumber.charAt(currentMatchedPosition) == readChar) {
                        currentMatchedPosition++;
                        resultedString.append(readChar);
                        if (currentMatchedPosition == targetNumber.length())
                            break;
                    } else {
                        currentMatchedPosition = 0;
                        resultedString.setLength(0);
                        while (map.hasRemaining() && readChar != fileGenerationProperties.getCharDelimiter())
                            readChar = (char) map.get();
                    }
                }

                if (currentMatchedPosition == targetNumber.length()) {
                    logger.info("Found \"{}\" in {}, {} sec",
                            resultedString,
                            path.getFileName().toString(),
                            TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - begin));
                    result.setCode(SearchResult.RESULT_CODE_OK);
                    result.getFileNames().add(path.getFileName().toString());
                } else {
                    logger.debug("Not found \"{}\" in {}, {} sec",
                            targetNumber,
                            path.getFileName().toString(),
                            TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - begin));
                    result.setCode(SearchResult.RESULT_CODE_NOT_FOUND);
                }
            } catch (Exception e) {
                logger.error("Error while searching \"{}\" in {}, {} sec",
                        targetNumber,
                        path.getFileName().toString(),
                        TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - begin),
                        e);
                result.setCode(SearchResult.RESULT_CODE_ERROR);
                result.setError(e.getMessage());
            }
            return result;
        }
    }


    private MappedByteBuffer getFileRegions(Path path) {
        FileChannel fileChannel;
        MappedByteBuffer map = null;
        try {
            fileChannel = FileChannel.open(path, StandardOpenOption.READ);
            map = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());
        } catch (IOException e) {
            logger.error("Can't get file region by fileChannel in {}.", path.getFileName().toString(), e);
        }
        return map;
    }

    @Override
    public Result findNumber(Integer number) {

        long begin = System.nanoTime();
        logger.info("Searching number {}...", number);

        String numberString = String.valueOf(number);
        ExecutorService executor = Executors.newFixedThreadPool(fileGenerationProperties.getNumber());

//        {
//            List<Path> paths = new ArrayList<>();
//            for (int fileNumber = 1; fileNumber < fileGenerationProperties.getNumber() + 1; fileNumber++) {
//                paths.add(Paths.get(fileGenerationProperties.getPath(), fileGenerationProperties.getPrefix() + fileNumber + ".txt"));
//            }
//            List<Future<Result>> futures = new ArrayList<>();
//            for (Path path : paths) {
//                futures.add(executor.submit(new InFileNumSeeker(path, numberString)));
//            }
//
//            List<Result> results = new ArrayList<>();
//            for (Future<Result> future : futures) {
//                try {
//                    results.add(future.get());
//                } catch (InterruptedException e) {
//                    Result errorResult = new Result();
//                    errorResult.setCode(SearchResult.RESULT_CODE_ERROR);
//                    errorResult.setError(e.getMessage());
//                    e.printStackTrace();
//                } catch (ExecutionException e) {
//                    Result errorResult = new Result();
//                    errorResult.setCode(SearchResult.RESULT_CODE_ERROR);
//                    errorResult.setError(e.getCause().getMessage());
//                    e.printStackTrace();
//                    results.add(errorResult);
//                }
//            }
//
//            Set<String> fileNames = new HashSet<>();
//            String errorMessage = null;
//            for (Result r : results) {
//                fileNames.addAll(r.getFileNames());
//                if (!StringUtils.isEmpty(r.getError())) {
//                    errorMessage = r.getError();
//                }
//            }
//            Result result = new Result();
//            if (!fileNames.isEmpty()) {
//                result.setCode(SearchResult.RESULT_CODE_OK);
//                result.getFileNames().addAll(fileNames);
//            } else if (!StringUtils.isEmpty(errorMessage)) {
//                result.setCode(SearchResult.RESULT_CODE_ERROR);
//                result.setError(errorMessage);
//            } else {
//                result.setCode(SearchResult.RESULT_CODE_NOT_FOUND);
//            }
//        }


//        { //770 sec
//        List<Path> collect = IntStream
//                .range(1, fileGenerationProperties.getNumber() + 1)
//                .mapToObj(fileNumber -> fileGenerationProperties.getPrefix() + fileNumber + ".txt")
//                .map(fileName -> Paths.get(fileGenerationProperties.getPath(), fileName))
//                .collect(Collectors.toList());
//        Result result = collect
//                .stream()
//                .parallel()
//                .map(path ->executor.submit(new InFileNumSeeker(path, numberString)))
//                .map(this::getResultFromFuture)
//                .reduce(new Result(), resultAccumulator);
//        }

        Result result = IntStream
                .range(1, fileGenerationProperties.getNumber() + 1)
                .mapToObj(fileNumber -> fileGenerationProperties.getPrefix() + fileNumber + ".txt")
                .map(fileName -> Paths.get(fileGenerationProperties.getPath(), fileName))
                /* hdd и в последовательной обработке грузится на 100%, смысла в параллельной нет */
                /* более того при тестах параллельная обработка медленее, видимо из за того что
                 * при параллельном чтении разных файлов hdd не может обеспечить эффективную скорость
                 * из за переключения области чтения */
//                .parallel()
                .map(path -> executor.submit(new InFileNumSeeker(path, numberString)))
                .map(this::getResultFromFuture)
                .reduce(new Result(), resultAccumulator);


        String lineSeparator = System.getProperty("line.separator");
        logger.info("Result: {}" +
                        "    code: {}{}" +
                        "   files: {}{}" +
                        "   error: {}{}" +
                        "    time: {}",
                lineSeparator,
                result.getCode(), lineSeparator,
                String.join(", ", result.getFileNames()), lineSeparator,
                result.getError(), lineSeparator,
                TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - begin));

        searchResultRepository.save(new SearchResult(result, number));
        return result;
    }

    private Result getResultFromFuture(Future<Result> resultFuture) {
        try {
            return resultFuture.get();
        } catch (InterruptedException e) {
            Result errorResult = new Result();
            errorResult.setCode(SearchResult.RESULT_CODE_ERROR);
            errorResult.setError(e.getMessage());
            e.printStackTrace();
            return errorResult;
        } catch (ExecutionException e) {
            Result errorResult = new Result();
            errorResult.setCode(SearchResult.RESULT_CODE_ERROR);
            errorResult.setError(e.getCause().getMessage());
            e.printStackTrace();
            return errorResult;
        }
    }

    private BinaryOperator<Result> resultAccumulator = (result1, result2) -> {
        String s = result1.getCode();
        if (SearchResult.RESULT_CODE_OK.equals(s)) {
            if (Collections.disjoint(result1.getFileNames(), result2.getFileNames())) {
                result1.getFileNames().addAll(result2.getFileNames());
                return result1;
            }
        } else if (SearchResult.RESULT_CODE_ERROR.equals(s)) {
            if (!result2.getCode().equals(SearchResult.RESULT_CODE_OK)) {
                return result1;
            }
        }
        return result2;
    };
}
