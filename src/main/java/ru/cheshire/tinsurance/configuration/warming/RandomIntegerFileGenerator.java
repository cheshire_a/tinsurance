package ru.cheshire.tinsurance.configuration.warming;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import ru.cheshire.tinsurance.configuration.properties.FileGenerationProperties;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Configuration
public class RandomIntegerFileGenerator {

    private final Logger logger = LoggerFactory.getLogger(RandomIntegerFileGenerator.class);

    private FileGenerationProperties fileGenerationProperties;

    public RandomIntegerFileGenerator(FileGenerationProperties generationProperties) {
        fileGenerationProperties = generationProperties;
    }

    @PostConstruct
    public void generateFiles() throws IOException {
        long begin;
        logger.info("Checking source files...");
        logger.debug("Source files configuration: ");
        logger.debug("    ---number:    {}", fileGenerationProperties.getNumber());
        logger.debug("    ---path:      {}", fileGenerationProperties.getPath());
        logger.debug("    ---prefix:    {}", fileGenerationProperties.getPrefix());
        logger.debug("    ---delimiter: {}", fileGenerationProperties.getCharDelimiter());
        logger.debug("    ---size:      {}", fileGenerationProperties.generation.getFileSize());
        logger.debug("    ---recreate:  {}", fileGenerationProperties.generation.getRecreate());

        begin = System.nanoTime();
        Files.createDirectories(Paths.get(fileGenerationProperties.getPath()));
        HashMap<String, String> a = new HashMap<>();
        a.put("", "");
        IntStream
                .range(1, fileGenerationProperties.getNumber() + 1)
                .mapToObj(fileNumber -> fileGenerationProperties.getPrefix() + fileNumber + ".txt")
                .map(fileName -> Paths.get(fileGenerationProperties.getPath(), fileName))
                .forEach(path -> {
                    logger.debug("Checking file {}, exist: {}, recreate: {}", path.getFileName().toString(), Files.exists(path), fileGenerationProperties.generation.getRecreate());
                    if (!Files.exists(path) || fileGenerationProperties.generation.getRecreate()) {
                        try (Writer w = Files.newBufferedWriter(path)) {
                            logger.debug("Writing to file {}...", path.getFileName().toString());
                            for (int j = 0; j < fileGenerationProperties.generation.getFileSize() / 12 * 1.0888889; j++) {
                                w.write(String.valueOf(ThreadLocalRandom.current().nextInt()) + fileGenerationProperties.getCharDelimiter());
                            }
                            w.write(String.valueOf(ThreadLocalRandom.current().nextInt()));
                            logger.debug("File (re-)created and filled.");
                        } catch (IOException e) {
                            e.printStackTrace();
                            logger.error("Can't create source file.", e);
                        }
                    }
                });
        logger.info("Source files ready.");
        logger.debug("{} min, {} sec, {} msec",
                TimeUnit.NANOSECONDS.toMinutes(System.nanoTime() - begin),
                TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - begin) % 60,
                TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - begin) % 1000
        );
    }
}
