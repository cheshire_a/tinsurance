package ru.cheshire.tinsurance.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Path;

@Configuration
@ConfigurationProperties(prefix = "data-file")
public class FileGenerationProperties {

    private Integer number = 2;
    private String path = "D:\\temp\\tinsurance";
    private String prefix = "randomInteger";
    private char charDelimiter = ',';

    public Generation generation;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public char getCharDelimiter() {
        return charDelimiter;
    }

    public void setCharDelimiter(char charDelimiter) {
        this.charDelimiter = charDelimiter;
    }

    public Generation getGeneration() {
        return generation;
    }

    public void setGeneration(Generation generation) {
        this.generation = generation;
    }

    public static class Generation {

        private Long fileSize = (long) (1024 * 1024);
        private Boolean recreate = false;

        public Long getFileSize() {
            return fileSize;
        }

        public void setFileSize(Long fileSize) {
            this.fileSize = fileSize;
        }

        public Boolean getRecreate() {
            return recreate;
        }

        public void setRecreate(Boolean recreate) {
            this.recreate = recreate;
        }
    }

}
