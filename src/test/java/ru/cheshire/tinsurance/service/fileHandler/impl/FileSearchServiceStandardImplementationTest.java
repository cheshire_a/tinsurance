package ru.cheshire.tinsurance.service.fileHandler.impl;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import ru.cheshire.tinsurance.configuration.properties.FileGenerationProperties;
import ru.cheshire.tinsurance.domain.SearchResult;
import ru.cheshire.tinsurance.service.repo.SearchResultRepository;
import ru.cheshire.tinsurance.soap.Result;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.concurrent.ThreadLocalRandom;

public class FileSearchServiceStandardImplementationTest {

    private final int testInt = 1590955344;

    private SearchResultRepository mockedSearchResultRepository = Mockito.mock(SearchResultRepository.class);

    private FileGenerationProperties getStub(Path file) {
        String tempFileName = file.getFileName().toString();
        FileGenerationProperties fileGenerationProperties = new FileGenerationProperties();
        fileGenerationProperties.setCharDelimiter(',');
        fileGenerationProperties.setNumber(1);
        fileGenerationProperties.setPath(file.getParent().getFileName().toString());
        fileGenerationProperties.setPrefix(tempFileName.substring(0, tempFileName.length() - 5));
        return fileGenerationProperties;
    }

    @Test
    public void findNumber_findTestInt_atStart() throws IOException {

        Path file = Paths.get("Temp", "findNumber_findTestInt_atStart1.txt");
        Files.createDirectories(file.getParent());
        BufferedWriter writer = Files.newBufferedWriter(file);
        StringBuilder sourceData = new StringBuilder();
        sourceData.append(testInt).append(",");
        ThreadLocalRandom
                .current()
                .ints(100)
                .forEach(randomInt -> sourceData.append(randomInt).append(","));
        sourceData.append(ThreadLocalRandom.current().nextInt());
        writer.write(sourceData.toString());
        writer.close();
        FileGenerationProperties fileGenerationProperties = getStub(file);
        fileGenerationProperties.setPrefix("findNumber_findTestInt_atStart");

        FileSearchServiceStandardImplementation testCase =
                new FileSearchServiceStandardImplementation(fileGenerationProperties, mockedSearchResultRepository);
        Result r = testCase.findNumber(testInt);


        Assert.assertEquals(SearchResult.RESULT_CODE_OK, r.getCode());
        Assert.assertEquals(Collections.singletonList(file.getFileName().toString()), r.getFileNames());
        Assert.assertTrue(StringUtils.isEmpty(r.getError()));

        Files.delete(file);
    }

    @Test
    public void findNumber_findTestInt_inMiddle() throws IOException {
        Path file = Paths.get("Temp", "findNumber_findTestInt_inMiddle1.txt");
        Files.createDirectories(file.getParent());
        BufferedWriter writer = Files.newBufferedWriter(file);
        StringBuilder sourceData = new StringBuilder();
        ThreadLocalRandom
                .current()
                .ints(50)
                .map(rand -> rand == testInt ? ThreadLocalRandom.current().nextInt(-testInt, testInt) : rand)
                .forEach(randomInt -> sourceData.append(randomInt).append(","));
        sourceData.append(testInt).append(",");
        ThreadLocalRandom
                .current()
                .ints(50)
                .map(rand -> rand == testInt ? ThreadLocalRandom.current().nextInt(-testInt, testInt) : rand)
                .forEach(randomInt -> sourceData.append(randomInt).append(","));
        sourceData.append(ThreadLocalRandom.current().nextInt());
        writer.write(sourceData.toString());
        writer.close();
        FileGenerationProperties fileGenerationProperties = getStub(file);
        fileGenerationProperties.setPrefix("findNumber_findTestInt_inMiddle");


        FileSearchServiceStandardImplementation testCase =
                new FileSearchServiceStandardImplementation(fileGenerationProperties, mockedSearchResultRepository);
        Result r = testCase.findNumber(testInt);


        Assert.assertEquals(SearchResult.RESULT_CODE_OK, r.getCode());
        Assert.assertEquals(Collections.singletonList(file.getFileName().toString()), r.getFileNames());
        Assert.assertTrue(StringUtils.isEmpty(r.getError()));

        Files.delete(file);
    }

    @Test
    public void findNumber_findTestInt_inTheEnd() throws IOException {
        Path file = Paths.get("Temp", "findNumber_findTestInt_inTheEnd1.txt");
        Files.createDirectories(file.getParent());
        BufferedWriter writer = Files.newBufferedWriter(file);
        StringBuilder sourceData = new StringBuilder();
        ThreadLocalRandom.current()
                .ints(100)
                .map(rand -> rand == testInt ? ThreadLocalRandom.current().nextInt(-testInt, testInt) : rand)
                .forEach(randomInt -> sourceData.append(randomInt).append(","));
        sourceData.append(testInt);
        writer.write(sourceData.toString());
        writer.close();
        FileGenerationProperties fileGenerationProperties = getStub(file);
        fileGenerationProperties.setPrefix("findNumber_findTestInt_inTheEnd");


        FileSearchServiceStandardImplementation testCase =
                new FileSearchServiceStandardImplementation(fileGenerationProperties, mockedSearchResultRepository);
        Result r = testCase.findNumber(testInt);


        Assert.assertEquals(SearchResult.RESULT_CODE_OK, r.getCode());
        Assert.assertEquals(Collections.singletonList(file.getFileName().toString()), r.getFileNames());
        Assert.assertTrue(StringUtils.isEmpty(r.getError()));

        Files.delete(file);
    }


    @Test
    public void findNumber_findTestInt_none() throws IOException {
        Path file = Paths.get("Temp", "findNumber_findTestInt_none1.txt");
        Files.createDirectories(file.getParent());
        BufferedWriter writer = Files.newBufferedWriter(file);
        StringBuilder sourceData = new StringBuilder();
        ThreadLocalRandom.current()
                .ints(100)
                .map(rand -> rand == testInt ? ThreadLocalRandom.current().nextInt(-testInt, testInt) : rand)
                .forEach(randomInt -> sourceData.append(randomInt).append(","));
        sourceData.append(-testInt);
        writer.write(sourceData.toString());
        writer.close();
        FileGenerationProperties fileGenerationProperties = getStub(file);
        fileGenerationProperties.setPrefix("findNumber_findTestInt_none");


        FileSearchServiceStandardImplementation testCase =
                new FileSearchServiceStandardImplementation(fileGenerationProperties, mockedSearchResultRepository);
        Result r = testCase.findNumber(testInt);


        Assert.assertEquals(SearchResult.RESULT_CODE_NOT_FOUND, r.getCode());
        Assert.assertTrue(CollectionUtils.isEmpty(r.getFileNames()));
        Assert.assertTrue(StringUtils.isEmpty(r.getError()));

        Files.delete(file);
    }


}